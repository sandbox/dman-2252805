Linker
======

Adds metadata and useful formatting to inline links to files.

Turns a link like **`[read this document]`**
into something like **`[read this document (report.pdf - PDF; 1.5M)]`**

This is recommended or required by a number of Web Standards Usability Guidelines,
such as those of the
[United Nations](http://www.un.org/webaccessibility/4_navigation/46_infoondocs.shtml)
or
[New Zealand Governemnt](https://webtoolkit.govt.nz/standards/web-usability-standard-1-1/#links-to-non-html-files)

This attempts to meet [WAI 11.3](http://www.w3.org/TR/WAI-WEBCONTENT/#gl-use-w3c)
 ("Provide information so that users may receive documents according to their preferences")


Usage
-----

This is a *text filter* so to enable it, admin the Formats at
`/admin/config/content/formats`

There, enable "Enhance inline links (linker)"

This filter should usually be run AFTER _"Convert URLS into links"_
and AFTER _"Correct URLs with Pathologic"_ (if used).


To enable file link enhancement, select _"Lookup file info"_ in the
_"Filter settings"_

Additional formatting settings are available:

* The **"Themable"** version will give you a file type icon,
  but not size by default - though this can be added by your themer.

* The custom **"Tokens"** version gives you full control of the markup and lets
  you choose the order etc of the icon, size, type, title and wrapper markup.

* **"View mode"** is available only if you are using *`file_entity.module`* and
  integrates with *`media.module`*.
  You can choose any managed "view mode" to embed
  ... though that's often overkill for inline text, it's powerful, and can
  help if you want to really manage your versioned documents by pointing
  users to the placeholder page for copyright or versioning info..

Working with automatic rewriting
--------------------------------

Although this is intended to make things automatic and easier most of the time,
this rewriting *can* conflict with your own markup, especially if using a
WYSIWYG or trying to do your own special styling sometimes.

The behavior can be disabled by either:

* Use any HTML markup inside your 'A' tag. EG, the following will be left alone:
  `<a href="file.pdf"><img src="preview.jpg"/></a>`

* Add `class="linker-disabled"` to the A tag.


See Also
--------

### Insert.module
WYSIWYG (and html) plugin to add links to attached file fields into your text.
A UI helper that embedded markup direct into your content, not a filter or enhanecr.

### Inline.module
An older token-based filter that would help embed links to attached files.
Focused on images, and overtaken in D6 by img_assist and more.
https://drupal.org/project/inline
